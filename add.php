<?php

session_start();
require_once './model/data.php'; // ajout connexion bdd 
// si la session existe pas soit si l'on est pas connecté on redirige
if (!isset($_SESSION['user'])) {
    header('Location:index.php');
    die();
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/styles.css">
    <title>Ajouter - Portfolio</title>
</head>

<body>
    <div class="wrapper-form">
    <a  class="disconnect" href="./controller/deconnexion.php">Déconnexion</a>
        <div class="boxform">
            <form action="./controller/insert.php" method="post" class="add-form" enctype="multipart/form-data">
                <label  class="loglabel" for="name">Nom du projet : </label>
                <input id="name" type="text" name="name" placeholder="Nom du projet.." required>

                <label class="loglabel" for="desc_pro">Description : </label>
                <input id="desc_pro" type="text" name="description" placeholder="Description.." required>

                <label class="loglabel" for="techno_used">Technologies : </label>
                <input id="techno_used" type="text" name="techno" placeholder="Technos.." required>

                <label class="loglabel" for="site_">URL : </label>
                <input id="site_" type="URL" name="site" placeholder="URL du site.." required>

                <label class="loglabel" for="site_img">URL de l'image : </label>
                <input id="site_img" type="URL" name="image" placeholder="URL de l'image.." required>

                <label class="loglabel" for="url_repo">URL du repo GIT : </label>
                <input id="url_repo" type="URL" name="repo" placeholder="URL du repo GIT.." required>

                <input type="submit" name="Envoyer" id="submit">
            </form>
        </div>
    </div>
</body>

</html>