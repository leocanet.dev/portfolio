<?php

session_start();
require_once '../model/data.php'; // ajout connexion bdd 
// si la session existe pas soit si l'on est pas connecté on redirige
if (!isset($_SESSION['user'])) {
    header('Location: ../index.php');
    die();
}

$id = $_GET["id"];

include '../model/data.php';
delete($id);

header("location: ../index.php#projets");
