<?php

include '../model/data.php';

session_start();
// si la session existe pas soit si l'on est pas connecté on redirige
if (!isset($_SESSION['user'])) {
    header('Location: ../index.php');
    die();
}

if (
    isset($_POST['name']) &&
    isset($_POST['description']) &&
    isset($_POST['techno']) &&
    isset($_POST['site']) &&
    isset($_POST['image']) &&
    isset($_POST['repo']) &&
    $_POST['name'] !== "" &&
    $_POST['description'] !== "" &&
    $_POST['techno'] !== "" &&
    $_POST['site'] !== "" &&
    $_POST['image'] !== "" &&
    $_POST['repo'] !== ""

) {
    $name = $_POST['name'];
    $description = $_POST['description'];
    $techno = $_POST['techno'];
    $site = $_POST['site'];
    $image = $_POST['image'];
    $repo = $_POST['repo'];

    insertPro($name, $description, $techno, $site, $image, $repo);

    header('location: ../index.php#projets');
};
