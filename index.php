<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="author" content="Leo Canet">
    <meta name="description" content="Portfolio de Leo Canet, apprenant Développeur web et web mobile">
    <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon/favicon-16x16.png">
    <link rel="manifest" href="./images/favicon/site.webmanifest">
    <link rel="stylesheet" href="./styles/styles.css">
    <title>Leo Canet - Portfolio</title>
</head>

<body>

    <header>
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                    <div id="logo"></div>

                <div class="collapse navbar-collapse ms-4" id="navbarTogglerDemo2">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#presentation"><span class="nav-span">01_</span>Présentation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#competences"><span class="nav-span">02_</span>Portfolio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact" tabindex="-1"><span class="nav-span">03_</span>Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main>
        <section id="intro">
                <div class="wrapper-block">
                    <span class="block"></span>
                    <div class="fadeup-enter-done" style="transition-delay: 100ms;">
                        <p class="hello">Hello world !</p>
                    </div>
                </div>
                <div class="wrapper-block">
                    <span class="block"></span>
                    <div class="fadeup-enter-done" style="transition-delay: 200ms;">
                        <h1 class="big-heading">Leo Canet.</h1>
                    </div>
                </div>
                <div class="wrapper-block">
                    <span class="block"></span>
                    <div class="fadeup-enter-done" style="transition-delay: 300ms;">
                        <h2 class="big-heading">Apprenant Développeur Web.</h2>
                    </div>
                </div>
                <div class="wrapper-block">
                    <span class="block"></span>
                    <div class="fadeup-enter-done" style="transition-delay: 400ms;">
                        <p>Je développe des applications web, des sites web tout en favorisant votre expérience utilisateur. Je peux évoluer à la fois sur la partie front-end et back-end d'une application web. Actuellement en formation chez <a href="https://simplon.co" target="_blank" rel="noreferrer">Simplon.co</a></p>
                    </div>
                </div>
                <div class="box_cv fadeup-enter-done" title="CV" style="transition-delay: 500ms;"><a class="link button-template" href="CV_CANET_Leo.pdf" target="_blank" rel="noreferrer">Aperçu</a></div>
        </section>

        <section id="presentation">
            <div class="box_pres reveal fade-bottom">
                <div>
                    <h3><span class="nav-span dot">01_</span> Présentation</h3>
                </div>
                <div class="box_inner">
                    <div id="box_text">
                        <p>
                            Bonjour! Je m'appelle Leo Canet, et j'adore faire des choses qui vivent sur Internet.
                            Mon intérêt pour le développement web a commencé en 2021, après quoi j'ai commencé une formation de développeur web et web mobile. En capacité de travailler autant sur la partie front-end que back-end d'une application web.
                        </p>
                        <p>
                            Voici quelques technologies avec lesquelles j'ai travaillé récemment ;
                        </p>
                        <ul class="list_techno">
                            <li>Javascript</li>
                            <li>PHP</li>
                            <li>SQL</li>
                            <li>Symfony</li>
                            <li>Wordpress</li>
                            <li>React</li>
                        </ul>
                    </div>
                    <div id="box_img">
                        <img class="me" src="./images/me3.webp" alt="me" />
                    </div>
                </div>
            </div>
        </section>

        <section id="competences">
            <?php
            include './model/data.php';
            $list_pro = getAllPro();
            ?>
            <div id="box_title">
                <h3><span class="nav-span dot">02_</span> Portfolio</h3>
            </div>
            <?php
            foreach ($list_pro as $f) {
            ?>
                <div class="wrapper fufu" id="box_<?php echo $f["id"] ?>">
                    <h4 class="name_pro"><i class="fas fa-angle-right fas-name"></i><?php echo $f["name"] ?></h4>
                    <div class="box_projects" id="reals">
                        <div class="box_one">
                            <p class="descr"><?php echo $f["description"] ?></p>
                            <p class="techno"><?php echo $f["techno"] ?></p>
                            <a target="_blank" href="<?php echo $f["site"] ?>"><svg xmlns="http://www.w3.org/2000/svg" role="img" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="site site-1">
                                    <title>External Link</title>
                                    <path d="M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6"></path>
                                    <polyline points="15 3 21 3 21 9"></polyline>
                                    <line x1="10" y1="14" x2="21" y2="3"></line>
                                </svg></a>
                            <a target="_blank" href="<?php echo $f["url_git"] ?>"><svg xmlns="http://www.w3.org/2000/svg" role="img" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="site">
                                    <title>GitHub</title>
                                    <path d="M9 19c-5 1.5-5-2.5-7-3m14 6v-3.87a3.37 3.37 0 0 0-.94-2.61c3.14-.35 6.44-1.54 6.44-7A5.44 5.44 0 0 0 20 4.77 5.07 5.07 0 0 0 19.91 1S18.73.65 16 2.48a13.38 13.38 0 0 0-7 0C6.27.65 5.09 1 5.09 1A5.07 5.07 0 0 0 5 4.77a5.44 5.44 0 0 0-1.5 3.78c0 5.42 3.3 6.61 6.44 7A3.37 3.37 0 0 0 9 18.13V22"></path>
                                </svg></a>
                        </div>
                        <div class="box_two">
                            <img class="img_projet" src="<?php echo $f["image"] ?>" alt="<?php echo $f["image"] ?>" width="100%" ;>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </section>

        <section id="contact" class="pt-1">
            <div>
                <h3><span class="nav-span dot">03_</span>Contact</h3>
            </div>
            <div class="box_form">
                <p class="contact-text">Toujours à la recherche de nouvelles opportunités, ma boîte de réception est toujours ouverte. Si vous avez une question ou que vous souhaitez simplement dire bonjour, je ferai de mon mieux pour vous répondre!</p>
                <div class="box_links">
                    <a title="Envoyer un mail" href="mailto:leocanet.dev@gmail.com"><i class="fas fa-envelope"></i></a>
                    <a title="LinkedIn" target="_blank" href="https://www.linkedin.com/in/leo-canet-9a9679166/"><i class="fab fa-linkedin"></i></a>
                    <a title="Gitlab" target="_blank" href="https://gitlab.com/leocanet.dev"><i class="fab fa-gitlab"></i></a>
                </div>
            </div>
        </section>
    </main>

    <footer class="flexcenter">
        <span class="copyright">&copy; Copyright 2022 - Leo CANET. Tous droits réservés.</span>
    </footer>
    <script src="https://kit.fontawesome.com/79730b03eb.js" crossorigin="anonymous"></script>
    <script src="./js/main.js"></script>
</body>

</html>