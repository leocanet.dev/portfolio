<?php

include 'debug.php';
include 'db.php';

function getAllPro()
{
    global $pdo;
    $req = $pdo->query('SELECT * FROM projets');
    return $req->fetchAll();
};

function insertPro($name, $description, $techno, $site, $image, $repo)
{
    global $pdo;
    $req = $pdo->prepare('INSERT INTO projets (name, description, techno, site, image, url_git)
 	VALUES (?, ?, ?, ?, ?, ?)');
    $req->execute([$name, $description, $techno, $site, $image, $repo]);
};

function delete($id)
{
    global $pdo;
    $req = $pdo->prepare('DELETE FROM projets WHERE id=?');
    $req->execute([$id]);
};
