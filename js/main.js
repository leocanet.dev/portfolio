// // Permet de fermer la navbar en version mobile quand on clique sur un nav-link
// const navLinks = document.querySelectorAll(".nav-item");
// const menuToggle = document.getElementById("navbarTogglerDemo2");
// const bsCollapse = new bootstrap.Collapse(menuToggle, {
//   toggle: false,
// });
// navLinks.forEach((l) => {
//   if (window.screen.width <= 991) {
//     l.addEventListener("click", () => {
//       bsCollapse.toggle();
//     });
//   }
// });

function reveal() {
  var reveals = document.querySelectorAll(".reveal");

  for (var i = 0; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    }
  }
}

window.addEventListener("scroll", reveal);