<?php
include 'debug.php';
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/styles.css">
    <title>Login - Portfolio</title>
</head>

<body>
    <div class="addform d-flex justify-content-center align-items-center">
        <div class="boxform container-fluid d-flex justify-content-center align-items-center">
            <form action="./controller/connexion.php" method="post" class="d-flex flex-column">
                <?php
                if (isset($_GET['login_err'])) {
                    $err = htmlspecialchars($_GET['login_err']);

                    switch ($err) {
                        case 'password':
                ?>
                            <div class="alert alert-danger">
                                <strong>Erreur : </strong> Mot de passe incorrect
                            </div>
                        <?php
                            break;

                        case 'email':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur : </strong> Email incorrect
                            </div>
                        <?php
                            break;

                        case 'already':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur : </strong> Compte non existant
                            </div>
                <?php
                            break;
                    }
                }
                ?>
                <p class="loglabel">Adresse email : </p>
                <input class="logtext" type="email" name="email" placeholder="Adresse email.." required="required">
                <p class="loglabel">Mot de passe : </p>
                <input type="password" name="password" placeholder="Mot de passe.." required="required">
                <input class="log" type="submit" name="envoi" value="Login">
            </form>
        </div>
    </div>
</body>

</html>